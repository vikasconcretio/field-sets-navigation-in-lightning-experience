@IsTest
public class SobjectListModelTest {
  /**	
	* @name SobjectListModelTest
	* @description used to test the wrapper class
	*/
    @isTest static void SobjectListModelTest(){
        Test.startTest();
        SobjectListModel sobj = new SobjectListModel();
        sobj.encoding = 'UTF';
        sobj.maxBatchSize = 200;
        SobjectListModel.cls_sobjects innerObj = new SobjectListModel.cls_sobjects();
        innerObj.name = 'Account';
        innerObj.layoutable = true;
        innerObj.triggerable = true;
        Test.stopTest();
    }
}