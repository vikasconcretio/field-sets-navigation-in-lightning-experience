/**
 *	@Author : Chirag Kochar
 *  @Type   : Controller class for the component FieldSetNavigation
 *  @Purpose : This class is reponsoible for creating, retrieving, updating and
 *			 deleting the Field Sets Through Lightning Component.
 * 
 */
public with sharing class FieldSetNavigationController {
	/**
     * @name ResponseWrapper
     * @type class
     * @purpose This wrapper class is used for storing the label, api name and description of the fields
	*/
	public class ResponseWrapper{
		@AuraEnabled
        public String Label {get;set;}
        @AuraEnabled
        public String Name {get;set;}
        @AuraEnabled
        public String Description {get;set;}
    }
    
    /**
     * @name getSObjectList
     * @purpose This method used to get the list of all objects
	 */
	@AuraEnabled	
	public static List<String> getSObjectList(){
        String sessionId;
        if(!test.isRunningTest()){
            PageReference GetSessionIdVF = Page.getSessionIdPage;
            String vfContent = GetSessionIdVF.getContent().toString();
            // Find the position of Start_Of_Session_Id and End_Of_Session_Id
            Integer startP = vfContent.indexOf('Start_Of_Session_Id') + 'Start_Of_Session_Id'.length(),
                endP = vfContent.indexOf('End_Of_Session_Id');
            // Get the Session Id
            sessionId = vfContent.substring(startP, endP);
        }
		List<string> SObjectList = new List<string>();
		//calling the webservice rest api for getting the all sobjects list which are applicable for creating custom field 
		//because we could only create field set only on those object which support custom field creation  
		Http http = new Http();
        httpRequest req = new httpRequest();
        req.setMethod('GET');
        //Set HTTPRequest header properties
        req.setHeader('content-type', 'application/json;charset=UTF-8');
        req.setHeader('Authorization', 'OAuth '+sessionId);
        req.setEndpoint('https://beunique-dev-ed.my.salesforce.com/services/data/v43.0/sobjects');
        HTTPResponse res = http.send(req);
        SobjectListModel wrapper = new SobjectListModel();
        wrapper =	(SobjectListModel) System.JSON.deserialize(res.getBody(), SobjectListModel.class);
        for(SobjectListModel.cls_sobjects model: wrapper.sobjects){
            if(model.triggerable && model.layoutable || model.custom){
                SObjectList.add(model.name);
            }
        }
		SObjectList.sort();
		return SObjectList ;
	}
	
    /**
     * @name - getFieldSetNames
     * @parameter - Object Name
     * @Purpose - This method used to get the list of all field set names which belongs to a particular Object
     * @return type - List of resonsewrapper consist of field set name,label,description of the fieldsets
     */
	@AuraEnabled	
	public static List<ResponseWrapper> getFieldSetNames(String typeName){
		List<ResponseWrapper> fieldSetnameList = new List<ResponseWrapper>();
		Schema.SObjectType targetType = Schema.getGlobalDescribe().get(typeName);
        Schema.DescribeSObjectResult describe = targetType.getDescribe();
        Map<String, Schema.FieldSet> fsMap = describe.fieldSets.getMap();
        List<String> fsNames = new List<String>();
        if(!fsMap.isEmpty())
        {
        	for(Schema.FieldSet fSet : fsmap.values()){
        		ResponseWrapper res = new ResponseWrapper();
        		res.Label = fSet.Label;
        		res.Name = fSet.Name;
        		res.Description = fSet.Description;
        		fieldSetnameList.add(res);
        	}
        }
        return fieldSetnameList;
	}
    
    @AuraEnabled
    Public static List<ResponseWrapper> responseList{get;set;}
    /**
     * @name  Class Method
     * @parameter  Object Name
     * @Purpose  This method used to get all fields in a particular Object
     * @return type  List of responsewrapper consist of field names and their labels
     */
    @AuraEnabled
    public static List<ResponseWrapper> getAllFields(String typeName){
        responseList = new List<ResponseWrapper>();
        Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Schema.SObjectType leadSchema = schemaMap.get(typeName);
        Map<String, Schema.SObjectField> fieldMap = leadSchema.getDescribe().fields.getMap(); 
        for (String fieldName: fieldMap.keySet()) {
           		ResponseWrapper res = new ResponseWrapper();
        		res.Label = fieldMap.get(fieldName).getDescribe().getLabel(); //label
        		res.Name = fieldName; //api name
           		responseList.add(res);
        }
        return responseList;
    }
    
     /**
     * @name getFieldsOfTheSelectedFieldSet
     * @parameter  Object Name and field set name
     * @Purpose This method used to get all fields of the existing field set related to particular object
     * @return type List of resonsewrapper consist of field names and their labels
     */
    @AuraEnabled 
    public static List<ResponseWrapper> getFieldsOfTheSelectedFieldSet(String typeName, String fieldSetName){
        List<ResponseWrapper> existingFieldList = new List<ResponseWrapper>();
        List<String> fieldList = new List<String>();
        Schema.SObjectType targetType = Schema.getGlobalDescribe().get(typeName);
        Schema.DescribeSObjectResult describe = targetType.getDescribe();
        Map<String, Schema.FieldSet> fsMap = describe.fieldSets.getMap();
        Schema.FieldSet fs = fsMap.get(fieldSetName);
        if(!Test.isRunningTest()){
            List<Schema.FieldSetMember> fieldSet = fs.getFields();
            for (Schema.FieldSetMember f: fieldSet) {
                    fieldList.add(f.getLabel());
            }
            for(ResponseWrapper res : responseList){
                if(fieldList.contains(res.Label)){
                    existingFieldList.add(res);
                }
            }
        }
        return existingFieldList;
    }
  	
     /**
     * @name  createService
     * @Purpose This method used to create the metadata service by using the MetadataService class and setting up the session id.
     * 			 This Method is used later for creating and updating the field set with the help of MetadataService class
     */
    public static MetadataService.MetadataPort createService(){
        MetadataService.MetadataPort service = new MetadataService.MetadataPort();
        service.SessionHeader = new MetadataService.SessionHeader_element();
        Blob b;
        // Refer to the Page
        PageReference GetSessionIdVF = Page.getSessionIdPage;
        // Get the content of the VF page
        if(!Test.isRunningTest()){
        	String vfContent = GetSessionIdVF.getContent().toString();
            // Find the position of Start_Of_Session_Id and End_Of_Session_Id
            Integer startP = vfContent.indexOf('Start_Of_Session_Id') + 'Start_Of_Session_Id'.length(),
            endP = vfContent.indexOf('End_Of_Session_Id');
            // Get the Session Id
           	String sessionId = vfContent.substring(startP, endP);
            service.SessionHeader.sessionId = sessionId;
        }
        else
        {	
            String sessionId = String.valueOf('00D7F0000049ETz!ARYAQOqJrJuNf93YIXgyVVD0Z9MWy6fIBBK9BM5zLueu9sWo7hpHgAAF3_DprgZbKtA__ePVkGWUP6e8RUzYBijS9mBMefvU');
            service.SessionHeader.sessionId = sessionId;
        }
        return service;
    }
	
     /**
     * @name createFieldSet
     * @parameter Object Name, field set name, label, description, fields list
     * @Purpose This method used to create the fieldSet by using the MetaDataService class method name createMetadata 
     * 			 by setting up the MetadataService.FieldSet attributes like fullname,label,DisplayedFields.
     * @return type String success
     */
    @AuraEnabled
	public static String createFieldSet(String objName,String fsName,String fsLabel, String description, List<String> fsList){
       	MetadataService.MetadataPort service = createService();
		List<MetadataService.FieldSetItem> myFieldSetList = new List<MetadataService.FieldSetItem>();
        // FieldSet
        MetadataService.FieldSet fieldSet = new MetadataService.FieldSet();
        fieldSet.fullName = objName+'.'+fsName;
        fieldSet.label = fsLabel;
        fieldSet.description = description;
        //assigning the fields in the fieldSetItem after that,this can be assign to the displayedFields variable of Fieldset 
        //so that this can be visible to user
        for(String fSetList : fsList)
        {
            MetadataService.FieldSetItem myAvailableField = new MetadataService.FieldSetItem();
            myAvailableField.field = fSetList;
            myAvailableField.isFieldManaged = true;
            myFieldSetList.add(myAvailableField);
        }
        fieldSet.displayedFields = new List<MetadataService.FieldSetItem>();
        for(MetadataService.FieldSetItem FieldSetList : myFieldSetList){
        fieldSet.displayedFields.add(FieldSetList);
        }
        // creating the field set by calling the method of the MetadataService i.e updateMetadataCreate
        List<MetadataService.SaveResult> results =
            service.createMetadata(
                new MetadataService.Metadata[] { fieldSet });
        handleSaveResults(results[0]);
        return 'success';
    }    
    
    /**
     * @name updateFieldSet
     * @parameter Object Name, field set name, label, description, fields list
     * @Purpose This method used to Update the fieldSet by using the MetaDataService class method name updateMetadata
     * @return type String success
     */
    @AuraEnabled
	public static String updateFieldSet(String objName,String fsName,String fsLabel, String description, List<String> fsList){
       	MetadataService.MetadataPort service = createService();
		List<MetadataService.FieldSetItem> myFieldSetList = new List<MetadataService.FieldSetItem>();
        // FieldSet
        MetadataService.FieldSet fieldSet = new MetadataService.FieldSet();
        fieldSet.fullName = objName+'.'+fsName;
        fieldSet.label = fsLabel;
        fieldSet.description = description;
        //assigning the fields in the fieldSetItem after that,this can be assign to the displayedFields variable of Fieldset 
        //so that this can be visible to user
        for(String fSetList : fsList)
        {
            MetadataService.FieldSetItem myAvailableField = new MetadataService.FieldSetItem();
            myAvailableField.field = fSetList;
            myAvailableField.isFieldManaged = true;
            myFieldSetList.add(myAvailableField);
        }
        //creating the list of displayed fields from the myFieldSetList list
        fieldSet.displayedFields = new List<MetadataService.FieldSetItem>();
        for(MetadataService.FieldSetItem FieldSetList : myFieldSetList){
        fieldSet.displayedFields.add(FieldSetList);
        }
        // Updateing the field set by calling the method of the MetadataService i.e updateMetadata
        List<MetadataService.SaveResult> results =
            service.updateMetadata(
                new MetadataService.Metadata[] { fieldSet });
        handleSaveResults(results[0]);
        return 'success';
    }    
    
    /**
     * @name deleteFieldSet
     * @parameter Object Name, field set name
     * @Purpose This method used to create the fieldSet by using the MetaDataService class method name deleteMetadata
     * @return type String success
     */
    @AuraEnabled
	public static String deleteFieldSet(String objName, String fsName){
        MetadataService.MetadataPort service = createService();
         String fieldSetName = objName+'.'+fsName;
        List<MetadataService.DeleteResult> results =
            service.deleteMetadata(
                 'FieldSet',new String[] { fieldSetName });
        return 'success';
    }
    
    public class MetadataServiceExamplesException extends Exception { }
    
    /**
     * @name handleSaveResults
     * @parameter  MetadataService.SaveResult saveResult
     * @Purpose This method used to handle the result after creating and updating the field set
     * @Exception MetadataServiceExamplesException
     */
    public static void handleSaveResults(MetadataService.SaveResult saveResult){
        // Nothing to see?
        if(saveResult==null || saveResult.success)
            return;
        // Construct error message and throw an exception
        if(saveResult.errors!=null)
        {
            List<String> messages = new List<String>();
            messages.add(
                (saveResult.errors.size()==1 ? 'Error ' : 'Errors ') +
                    'occured processing component ' + saveResult.fullName + '.');
            for(MetadataService.Error error : saveResult.errors)
                messages.add(
                    error.message + ' (' + error.statusCode + ').' +
                    ( error.fields!=null && error.fields.size()>0 ?
                        ' Fields ' + String.join(error.fields, ',') + '.' : '' ) );
            if(messages.size()>0)
                throw new MetadataServiceExamplesException(String.join(messages, ' '));
        }
        if(!saveResult.success)
            throw new MetadataServiceExamplesException('Request failed with no specified error.');
    }
    
}