public class SobjectListModel {
    public String encoding;	//UTF-8
    public Integer maxBatchSize;	//200
    public cls_sobjects[] sobjects;
    public class cls_sobjects {
        public boolean layoutable;
        public String name;	//AcceptedEventRelation
        public boolean triggerable;
        public boolean custom;
    }
}