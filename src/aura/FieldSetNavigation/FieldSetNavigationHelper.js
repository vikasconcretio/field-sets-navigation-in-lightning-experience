({
    //for getting all the fields of the selected object
    getAllFields : function(component,event){
        var objName = component.get("v.objectName");
        var action = component.get("c.getAllFields");
        action.setParams({
            "typeName" : objName
        });
        action.setCallback(this, function(response){
			var state = response.getState();
            if(state == "SUCCESS"){
				var fList = response.getReturnValue();
                var fieldList = [];
                for(var i=0; i< fList.length ; i++){
                    fieldList.push({label : fList[i].Label, value : fList[i].Name});
                }
                fieldList.sort(function(a, b) {
                   return a.label.localeCompare(b.label);
                });
                component.set("v.listOptions", fieldList);
            }
        });
        $A.enqueueAction(action);
    },
    //for getting the field set of the selected object
    getSelectedObjectFieldSets : function(component, event){
      var selectedObject = component.find("mySelect").get("v.value");
		if(selectedObject){
            component.set("v.objectName", selectedObject);
            component.set("v.showTable",true);
			component.set("v.visibleNewButtonSection", true);
		}
        else{
            component.set("v.showTable",false);
            component.set("v.visibleNewButtonSection", false);
			component.set("v.visibleFieldSetList", false);
        }
		var action = component.get("c.getFieldSetNames");
		action.setParams({
            "typeName" : selectedObject
        });
		action.setCallback(this, function(response){
			var state = response.getState();
			if(state == "SUCCESS"){
				var fsList = response.getReturnValue();
                if(fsList.length > 0)
                {
                    component.set("v.visibleFieldSetList", true);
                    component.set("v.visibleNoFieldSet",false)
                    var setFsList = [];
                        for(var i=0 ;i < fsList.length ; i++)
                        {
                            setFsList.push(fsList[i]);
                        }
                        component.set("v.fieldSetNames", setFsList);
                }
                else
                {
                    component.set("v.visibleFieldSetList", false);
                    component.set("v.visibleNoFieldSet", true);
                    component.set("v.visibleNewButtonSection", true);
                }
			}
		});
			$A.enqueueAction(action);
    },
    //close new modal box
    closeModalBox : function(component,event) {
        var cmpTarget = component.find('Modalbox');
		var cmpBack = component.find('ModalBackdrop');
		$A.util.removeClass(cmpBack,'slds-backdrop--open');
		$A.util.removeClass(cmpTarget, 'slds-fade-in-open'); 
	},
    //open new modal box
    openModalBox : function(component,event) {
        component.find("label").set("v.value", " ");
        component.set("v.apiName"," ");
        component.find("desc").set("v.value"," ");
        var cmpTarget = component.find('Modalbox');
		var cmpBack = component.find('ModalBackdrop');
		$A.util.addClass(cmpTarget, 'slds-fade-in-open');
		$A.util.addClass(cmpBack, 'slds-backdrop--open');
	},
    //canceling the edit modal box
    cancelModalBox : function(component,event) {
		var cmpTarget = component.find('FieldModalbox');
		var cmpBack = component.find('Modalbackdrop');
		$A.util.removeClass(cmpBack,'slds-backdrop--open');
		$A.util.removeClass(cmpTarget, 'slds-fade-in-open'); 
        var label = component.find("label").get("v.value");
        var name = component.find("apiName").get("v.value");
        var desc = component.find("desc").get("v.value");
        if(label && name && desc)
        {
            component.find("label").set("v.value", " ");
            component.set("v.apiName"," ");
            component.find("desc").set("v.value"," ");
        }
        //clearing out the options and the values of the dual list box on canceling the modal box
        component.set("v.listOptions", " ");
        component.set("v.defaultOptions"," ");
	},
    //for open the edit modal box
    openEditFieldModalBox : function(component,event) {
        this.closeModalBox(component,event);
		var cmpTarget = component.find('FieldModalbox');
		var cmpBack = component.find('Modalbackdrop');
		$A.util.addClass(cmpTarget, 'slds-fade-in-open');
		$A.util.addClass(cmpBack, 'slds-backdrop--open');
	},
    //generating the api name with the help of label name
    apiName : function(component,event)
    {
        var getLabel = component.find("label").get("v.value");
        var label = getLabel.trim();
        var apiname = label.split(' ').join('_');
        component.set("v.apiName",apiname);
    },
    //getting field set of the selected object
    getFieldsOfTheSelectedFieldSet : function(component, fieldSetName){
        this.getAllFields(component,event);
        var objectName = component.get("v.objectName");
        var action = component.get("c.getFieldsOfTheSelectedFieldSet");
            action.setParams({
                "typeName" : objectName,
                "fieldSetName" : fieldSetName
            });
             action.setCallback(this, function(response){
                var state = response.getState();
                if(state == "SUCCESS"){
                    var fList = response.getReturnValue();
                    var fieldSetFields = [];
                    for(var i=0; i< fList.length ; i++){
                        if(i==0){component.set("v.showSave", true);}
                        fieldSetFields.push(fList[i].Name);
                    }
                    component.set("v.defaultOptions", fieldSetFields);
                }
            });
            $A.enqueueAction(action);
     },
    //for getting the value of the dual list box
    onChange : function(component, event){
        var selectedOptionsList = event.getParam("value");
        if(selectedOptionsList.length > 0){
            component.set("v.showSave", true)
        }else{
            component.set("v.showSave", false)
        }
            
        component.set("v.selectedFields",selectedOptionsList);
    },
    //for handling the save action it works on the new creation and on updation
    handleSaveFieldList : function(component, event){
        var objectName = component.get("v.objectName");   
        var label = component.find("label").get("v.value");
        var name = component.find("apiName").get("v.value");
        var desc = component.find("desc").get("v.value");
        var fList = component.get("v.selectedFields");
        var actionForCreateUpdate = component.get("v.flag");
        if(actionForCreateUpdate){
            var action = component.get("c.createFieldSet");
            action.setParams({
                "objName" : objectName,
                "fsName" : name,
                "fsLabel" : label,
                "description" : desc,
                "fsList" : fList
            });
            action.setCallback(this, function(response){
                var state = response.getState();
                if(state == "SUCCESS"){
                    this.cancelModalBox(component, event);
                    this.getSelectedObjectFieldSets(component, event);
                }
                else{
                    this.cancelModalBox(component, event);
                }
            });
            $A.enqueueAction(action);
            component.set("v.flag",false);
            component.set("v.listOptions", " ");
            component.set("v.defaultOptions"," ");
            component.set("v.selectedFields"," ");
        }
        else{
            var fieldSetList = component.get("v.fieldSetNames");
            var fLabel;
            var fDesc;
            for(var i=0; i< fieldSetList.length; i++){
                if(fieldSetList[i].Name == name){
                    fLabel = fieldSetList[i].Label;
                    fDesc = fieldSetList[i].Description;
                }
            }
            var action = component.get("c.updateFieldSet");
            action.setParams({
                "objName" : objectName,
                "fsName" : name,
                "fsLabel" : fLabel,
                "description" : fDesc,
                "fsList" : fList
            });
            action.setCallback(this, function(response){
                var state = response.getState();
                if(state == "SUCCESS"){
                    this.cancelModalBox(component, event);
                    this.getSelectedObjectFieldSets(component, event);
                }
                else{
                    this.cancelModalBox(component, event);
                }
            });
            $A.enqueueAction(action);
            component.set("v.listOptions", " ");
            component.set("v.defaultOptions"," ");
            component.set("v.selectedFields"," ");
        }
    },
    //for deleting the field set
    handleDeleteFieldSet : function(component, fieldSetName){
       var objectName = component.get("v.objectName");
       var action = component.get("c.deleteFieldSet") 
       action.setParams({
            "objName" : objectName,
            "fsName" : fieldSetName
       });
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state == "SUCCESS"){
                this.getSelectedObjectFieldSets(component, event);
            }
        });
        $A.enqueueAction(action);
    }
})